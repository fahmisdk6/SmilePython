# Library for urlconnection and web scraping
import urllib.request
from bs4 import BeautifulSoup

# Get the page ready , access quora question
quora = "https://www.quora.com/Homework-Question-How-do-I-write-a-program-that-produces-the-following-output-1"
page = urllib.request.urlopen(quora)
soup = BeautifulSoup(page, "html.parser")

# Scraping content from specified div's class
div = soup.find("div", class_="question_details_text inline_editor_content")
strContent = ""
for content in div.span.contents:
    strContent += str(content)

# Data cleaning
strContent = strContent.replace("<br/>", "\n")
strContent = strContent.split("\n\n")[0]

# Show Output
print(strContent)


