### What is this?

SmilePython is an example solution to a question at quora that says:

How do I write a program that produces the following output?
Smile!Smile!Smile!
Smile!Smile!
Smile!

(Original link: https://www.quora.com/Homework-Question-How-do-I-write-a-program-that-produces-the-following-output-1)

This solution is written in python language with help from dependency like `BeautifulSoup` and `urllib` for webscrapping and url connection.


### Setup

1. Install python v 3.x
2. Install pip
3. Install beautifulSoup via pip 
4. Run 